'((asset-directories . ("assets/fonts" "assets/images" "assets/sounds"))
  (bundle-name . "bonnie-bee-1.0-x86_64")
  (code . "bonnie-bee.scm")
  (ignore-files . ("\\.xcf$" "\\.xm$"))
  (launcher-name . "bonnie-bee"))
