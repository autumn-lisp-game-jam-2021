(define-module (bonnie-bee assets)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics font)
  #:use-module (chickadee graphics texture)
  #:use-module (starling asset)
  #:export (monogram-font
            background-image
            particle-image
            darkness-image
            lightness-image
            chickadee-image
            bee-atlas
            bullet-atlas
            popcorn-atlas
            flower-atlas
            turret-atlas
            moth-atlas
            beetle-atlas
            explosion-sound
            pickup-sound
            enemy-shoot-sound
            enemy-hit-sound
            player-death-sound
            player-shoot-sound
            player-bomb-sound
            pollen-release-sound
            hehehe-sound
            alarm-sound
            intro-music
            main-music))

(define (scope-datadir file-name)
  (let ((prefix (or (getenv "BONNIE_BEE_DATADIR") (getcwd))))
    (string-append prefix "/" file-name)))

(define-asset monogram-font (load-font (scope-datadir "assets/fonts/monogram_extended.ttf") 12))
(define-asset background-image (load-image (scope-datadir "assets/images/background.png")))
(define-asset particle-image (load-image (scope-datadir "assets/images/particle.png")))
(define-asset darkness-image (load-image (scope-datadir "assets/images/darkness.png")))
(define-asset lightness-image (load-image (scope-datadir "assets/images/lightness.png")))
(define-asset chickadee-image (load-image (scope-datadir "assets/images/chickadee.png")))
(define-asset bee-atlas (load-tileset (scope-datadir "assets/images/bee.png") 32 32))
(define-asset bullet-atlas (load-tileset (scope-datadir "assets/images/bullets.png") 16 16))
(define-asset flower-atlas (load-tileset (scope-datadir "assets/images/flower.png") 64 64))
(define-asset popcorn-atlas (load-tileset (scope-datadir "assets/images/popcorn.png") 32 32))
(define-asset turret-atlas (load-tileset (scope-datadir "assets/images/turret.png") 64 64))
(define-asset moth-atlas (load-tileset (scope-datadir "assets/images/moth.png") 64 64))
(define-asset beetle-atlas (load-tileset (scope-datadir "assets/images/beetle.png") 128 64))
(define-asset explosion-sound (load-audio (scope-datadir "assets/sounds/explosion.wav")))
(define-asset pickup-sound (load-audio (scope-datadir "assets/sounds/pickup.wav")))
(define-asset enemy-shoot-sound (load-audio (scope-datadir "assets/sounds/enemy-shoot.wav")))
(define-asset enemy-hit-sound (load-audio (scope-datadir "assets/sounds/enemy-hit.wav")))
(define-asset player-death-sound (load-audio (scope-datadir "assets/sounds/player-death.wav")))
(define-asset player-shoot-sound (load-audio (scope-datadir "assets/sounds/player-shoot.wav")))
(define-asset player-bomb-sound (load-audio (scope-datadir "assets/sounds/player-bomb.wav")))
(define-asset pollen-release-sound (load-audio (scope-datadir "assets/sounds/pollen-release.wav")))
(define-asset hehehe-sound (load-audio (scope-datadir "assets/sounds/hehehe.wav")))
(define-asset alarm-sound (load-audio (scope-datadir "assets/sounds/alarm.wav")))
(define-asset intro-music (load-audio (scope-datadir "assets/sounds/intro.ogg") #:mode 'stream))
(define-asset main-music (load-audio (scope-datadir "assets/sounds/main.ogg") #:mode 'stream))
