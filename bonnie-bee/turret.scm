(define-module (bonnie-bee turret)
  #:use-module (bonnie-bee actor)
  #:use-module (bonnie-bee assets)
  #:use-module (bonnie-bee bullet)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics particles)
  #:use-module (chickadee math)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (chickadee utils)
  #:use-module (oop goops)
  #:use-module (starling asset)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:export (<turret>))

(define-class <turret> (<grounded> <damageable> <actor>))

(define-method (on-boot (turret <turret>))
  (attach-to turret
             (make <animated-sprite>
               #:atlas turret-atlas
               #:origin (vec2 32.0 32.0)
               #:animations `((default . ,(make <animation>
                                            #:frames #(0 1 2 3)
                                            #:frame-duration .15))))))

(define-method (on-collide (turret <turret>) (bullet <bullet>))
  (cond
   ((player-primary-bullet? bullet)
    (damage turret 1)
    (kill-bullet bullet)
    #t)
   ((player-bomb-bullet? bullet)
    (damage turret 5)
    (kill-bullet bullet))
   (else #f)))

(define-method (on-death (turret <turret>))
  (audio-play (asset-ref explosion-sound))
  (let ((p (position turret)))
    (add-particle-emitter (particles (particles (parent turret)))
                          (make-particle-emitter (make-rect (vec2-x p) (vec2-y p) 1.0 1.0)
                                                 10 3))
    (for-range ((i 8))
      (let ((theta (- (* (random:uniform) (/ pi -2.0)) (/ pi 4.0)))
            (speed (+ (* (random:uniform) 1.0) 1.0)))
        (add-bullet (bullets (parent turret))
                    medium-enemy-bullet p
                    (vec2 (* (cos theta) speed)
                          (* (sin theta) speed)))))))
