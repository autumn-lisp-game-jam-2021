(define-module (bonnie-bee popcorn)
  #:use-module (bonnie-bee actor)
  #:use-module (bonnie-bee assets)
  #:use-module (bonnie-bee bullet)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics particles)
  #:use-module (chickadee math)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (chickadee utils)
  #:use-module (oop goops)
  #:use-module (starling asset)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:export (<popcorn>))

(define-class <popcorn> (<damageable> <actor>))

(define-method (on-boot (popcorn <popcorn>))
  (attach-to popcorn
             (make <animated-sprite>
               #:atlas popcorn-atlas
               #:origin (vec2 16.0 16.0)
               #:animations `((default . ,(make <animation>
                                            #:frames #(0 1)
                                            #:frame-duration .25))))))

(define-method (on-collide (popcorn <popcorn>) (bullet <bullet>))
  (if (player-bullet? bullet)
      (begin
        (damage popcorn 1)
        (kill-bullet bullet)
        #t)
      #f))

(define-method (on-death (popcorn <popcorn>))
  (audio-play (asset-ref explosion-sound))
  (let ((p (position popcorn)))
    (add-particle-emitter (particles (particles (parent popcorn)))
                          (make-particle-emitter (make-rect (vec2-x p) (vec2-y p) 1.0 1.0)
                                                 4 3))))
