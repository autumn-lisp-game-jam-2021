(define-module (bonnie-bee common)
  #:use-module (bonnie-bee assets)
  #:use-module (chickadee game-loop)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics viewport)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (starling scene)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:export (%window-width
            %window-height
            %game-width
            %game-height
            set-cameras!
            steps
            fade-in
            fade-out))

(define %window-width 960)
(define %window-height 720)
(define %game-width 320)
(define %game-height 240)

(define-method (set-cameras! (scene <scene-2d>))
  (set! (cameras scene)
        (list (make <camera-2d>
                #:resolution (vec2 %game-width %game-height)
                #:viewport (make-viewport 0 0 %window-width %window-height
                                          #:clear-color black)))))

(define (steps n)
  (* n (current-timestep)))

(define-method (fade-in (scene <scene-2d>) duration)
  (let ((bg (make <sprite>
              #:rank 999
              #:texture darkness-image)))
    (attach-to scene bg)
    (tween duration 1.0 0.0
           (lambda (a)
             (set! (tint bg) (transparency a))))
    (detach bg)))

(define-method (fade-out (scene <scene-2d>) duration)
  (let ((bg (make <sprite>
              #:rank 999
              #:texture darkness-image)))
    (attach-to scene bg)
    (tween duration 0.0 1.0
           (lambda (a)
             (set! (tint bg) (transparency a))))
    (detach bg)))
