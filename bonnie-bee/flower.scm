(define-module (bonnie-bee flower)
  #:use-module (bonnie-bee actor)
  #:use-module (bonnie-bee assets)
  #:use-module (bonnie-bee bullet)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics particles)
  #:use-module (chickadee math)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (chickadee utils)
  #:use-module (oop goops)
  #:use-module (starling asset)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:export (<flower>
            pollen-enabled?
            enable-pollen!
            disable-pollen!))

(define-class <flower> (<grounded> <damageable> <actor>)
  (emit-pollen? #:allocation #:class #:init-value #f))

(define-class <pollinated-flower> (<grounded> <actor>))

(define (pollen-enabled?)
  (class-slot-ref <flower> 'emit-pollen?))

(define (enable-pollen!)
  (class-slot-set! <flower> 'emit-pollen? #t))

(define (disable-pollen!)
  (class-slot-set! <flower> 'emit-pollen? #f))

(define-method (on-boot (flower <flower>))
  (attach-to flower
             (make <animated-sprite>
               #:atlas flower-atlas
               #:origin (vec2 32.0 32.0)
               #:animations `((default . ,(make <animation>
                                           #:frames #(0 1 0 2)
                                           #:frame-duration .45))))))

(define-method (on-boot (flower <pollinated-flower>))
  (attach-to flower
             (make <atlas-sprite>
               #:atlas flower-atlas
               #:origin (vec2 32.0 32.0)
               #:index 3)))

(define-method (on-collide (flower <flower>) (bullet <bullet>))
  (cond
   ((player-primary-bullet? bullet)
    (damage flower 1)
    (kill-bullet bullet)
    #t)
   ((player-bomb-bullet? bullet)
    (damage flower 5)
    (kill-bullet bullet))
   ((and (not (pollen-enabled?)) (enemy-bullet? bullet))
    (damage flower 1000)
    (kill-bullet bullet))
   (else #f)))

(define-method (on-death (flower <flower>))
  (let ((p (position flower)))
    (if (pollen-enabled?)
        (begin
          (audio-play (asset-ref pollen-release-sound))
          (for-range ((i 16))
            (let ((theta (- (* (- (random:uniform) 0.5) (/ pi 3.0))
                            (/ pi 2.0)))
                  (speed (+ (* (random:uniform) 1.0) 1.0)))
              (add-bullet (bullets (parent flower))
                          pollen-pickup p
                          (vec2 (* (cos theta) speed)
                                (* (sin theta) speed)))))
          (spawn (parent flower)
                 (make <pollinated-flower>
                   #:rank 1
                   #:position (position flower)
                   #:hitbox (make-rect -1.0 -1.0 2.0 2.0))))
        (begin
          (audio-play (asset-ref explosion-sound))
          (add-particle-emitter (particles (particles (parent flower)))
                                (make-particle-emitter (make-rect (vec2-x p) (vec2-y p) 1.0 1.0)
                                                       10 5))))))
