(define-module (bonnie-bee moth)
  #:use-module (bonnie-bee actor)
  #:use-module (bonnie-bee assets)
  #:use-module (bonnie-bee bullet)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics particles)
  #:use-module (chickadee math)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (chickadee utils)
  #:use-module (oop goops)
  #:use-module (starling asset)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:export (<moth>))

(define-class <moth> (<damageable> <actor>))

(define-method (on-boot (moth <moth>))
  (attach-to moth
             (make <animated-sprite>
               #:atlas moth-atlas
               #:origin (vec2 32.0 32.0)
               #:animations `((default . ,(make <animation>
                                            #:frames #(0 1 0 2)
                                            #:frame-duration .1))))))

(define-method (on-collide (moth <moth>) (bullet <bullet>))
  (cond
   ((player-primary-bullet? bullet)
    (damage moth 1)
    (kill-bullet bullet)
    #t)
   ((player-bomb-bullet? bullet)
    (damage moth 10)
    (kill-bullet bullet))
   (else #f)))

(define-method (on-death (moth <moth>))
  (audio-play (asset-ref explosion-sound))
  (let ((p (position moth)))
    (add-particle-emitter (particles (particles (parent moth)))
                          (make-particle-emitter (make-rect (vec2-x p) (vec2-y p) 1.0 1.0)
                                                 4 3))))
