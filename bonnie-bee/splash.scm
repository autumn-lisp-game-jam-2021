(define-module (bonnie-bee splash)
  #:use-module (bonnie-bee assets)
  #:use-module (bonnie-bee common)
  #:use-module (bonnie-bee game)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (starling kernel)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:use-module (starling scene)
  #:export (launch-game))

(define-class <splash> (<scene-2d>))

(define %text-color (rgb #x181425))

(define-method (on-boot (splash <splash>))
  (set-cameras! splash)
  (attach-to splash
             (make <sprite>
               #:texture lightness-image)
             (make <sprite>
               #:texture chickadee-image
               #:position (vec2 (/ %game-width 2.0)
                                (/ %game-height 2.0))
               #:origin (vec2 8.0 8.0)
               #:scale (vec2 4.0 4.0))
             (make <label>
               #:rank 1
               #:position (vec2 (/ %game-width 2.0) 70.5)
               #:font monogram-font
               #:color %text-color
               #:align 'center
               #:vertical-align 'center
               #:text "Made with Chickadee")
             (make <label>
               #:rank 1
               #:position (vec2 (/ %game-width 2.0) 50.5)
               #:font monogram-font
               #:color %text-color
               #:align 'center
               #:vertical-align 'center
               #:text "https://dthompson.us/projects/chickadee.html")))

(define-method (on-enter (splash <splash>))
  (run-script splash
    (unless (getenv "SKIP_SPLASH")
      (fade-in splash 1.0)
      (sleep 1.0)
      (fade-out splash 1.0))
    (replace-scene (current-kernel) (make <game>))))

(define (launch-game)
  (boot-kernel (make <kernel>
                 #:window-config (make <window-config>
                                   #:title "Bonnie Bee and the Pesticidal Tendencies - Autumn Lisp Game Jam 2021"
                                   #:width %window-width
                                   #:height %window-height))
               (lambda () (make <splash>))))
