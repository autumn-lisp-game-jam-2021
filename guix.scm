(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix build-system gnu)
             (guix download)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (gnu packages)
             (gnu packages audio)
             (gnu packages autotools)
             (gnu packages fontutils)
             (gnu packages gl)
             (gnu packages guile)
             (gnu packages image)
             (gnu packages maths)
             (gnu packages mp3)
             (gnu packages pkg-config)
             (gnu packages readline)
             (gnu packages sdl)
             (gnu packages texinfo)
             (gnu packages xiph))

(define target-guile guile-3.0-latest)

(define guile3.0-opengl
  (package
    (inherit guile-opengl)
    (inputs
     (modify-inputs (package-inputs guile-opengl)
       (replace "guile" target-guile)))
    (native-inputs
     (modify-inputs (package-native-inputs guile-opengl)
       (append autoconf automake)))
    (arguments
     (substitute-keyword-arguments (package-arguments guile-opengl)
       ((#:phases phases)
        `(modify-phases ,phases
           (delete 'patch-makefile)
           (add-before 'bootstrap 'patch-configure.ac
             (lambda _
               ;; The Guile version check doesn't work for the 3.0
               ;; pre-release, so just remove it.
               (substitute* "configure.ac"
                 (("GUILE_PKG\\(\\[2.2 2.0\\]\\)") ""))
               (substitute* "Makefile.am"
                 (("\\$\\(GUILE_EFFECTIVE_VERSION\\)") "3.0")
                 (("ccache") "site-ccache"))
               #t))
           (replace 'bootstrap
             (lambda _
               (invoke "autoreconf" "-vfi")))))))))

(define guile-sdl2
  (let ((commit "e9a7f5e748719ce5b6ccd08ff91861b578034ea6"))
    (package
      (name "guile-sdl2")
      (version (string-append "0.7.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/guile-sdl2.git")
                      (commit commit)))
                (sha256
                 (base32
                  "0ay7mcar8zs0j5rihwlzi0l46vgg9i93piip4v8a3dzwjx3myr7v"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'bootstrap
                      (lambda _
                        (invoke "sh" "bootstrap"))))))
      (native-inputs (list autoconf automake pkg-config texinfo))
      (inputs (list target-guile sdl2))
      (synopsis "Guile bindings for SDL2")
      (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
      (home-page "https://git.dthompson.us/guile-sdl2.git")
      (license license:lgpl3+))))

(define chickadee
  (let ((commit "5ed490cac58d9f548bc141cc334c25dea9bfcaf2"))
    (package
     (name "chickadee")
     (version (string-append "0.8.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/chickadee.git")
                    (commit commit)))
              (sha256
               (base32
                "1wq1v19qp2q31ybjqzw0xccyw5v03hh9y79z8rjd6l83vbp1nvkx"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
                       (add-after 'unpack 'bootstrap
                                  (lambda _
                                    (invoke "sh" "bootstrap"))))))
     (native-inputs (list autoconf automake pkg-config texinfo))
     (inputs (list freetype
                   libjpeg-turbo
                   libpng
                   libvorbis
                   mpg123
                   openal
                   readline
                   target-guile))
     (propagated-inputs (list guile3.0-opengl guile-sdl2))
     (synopsis "Game development toolkit for Guile Scheme")
     (description "Chickadee is a game development toolkit for Guile
Scheme.  It contains all of the basic components needed to develop
2D/3D video games.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license license:gpl3+))))

(define starling
  (let ((commit "bd8b5d1f0f7c043c4bef2191a3def7b9fa3efacf"))
    (package
     (name "starling")
     (version (string-append "0.1.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/starling.git")
                    (commit commit)))
              (sha256
               (base32
                "15hmbb8ksl26hp0cqar08i84hn8x4mrk4agq5v2gd69l23lrkq7k"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
                       (add-after 'unpack 'bootstrap
                                  (lambda _
                                    (invoke "sh" "bootstrap"))))))
     (native-inputs (list autoconf automake pkg-config texinfo))
     (inputs (list target-guile))
     (propagated-inputs (list chickadee guile-sdl2))
     (synopsis "Game engine for Guile Scheme")
     (description "Starling is a game engine that I haven't released
yet.")
     (home-page "https://git.dthompson.us/starling.git")
     (license license:gpl3+))))

(define %source-dir (dirname (current-filename)))

(package
  (name "autum-lisp-game-jam-2021")
  (version "0.1")
  (source (local-file %source-dir
                      #:recursive? #t
                      #:select? (git-predicate %source-dir)))
  (build-system gnu-build-system)
  (arguments
   `(#:modules (((guix build guile-build-system)
                 #:select (target-guile-effective-version))
                (ice-9 match)
                (ice-9 ftw)
                ,@%gnu-build-system-modules)
     #:imported-modules ((guix build guile-build-system) ,@%gnu-build-system-modules)
     #:make-flags '("GUILE_AUTO_COMPILE=0")
     #:phases
     (modify-phases %standard-phases
       (add-after 'unpack 'bootstrap
         (lambda _
           (invoke "sh" "bootstrap")))
       (add-after 'install 'wrap-script
         (lambda* (#:key inputs outputs #:allow-other-keys)
           (let* ((out  (assoc-ref outputs "out"))
                  (version (target-guile-effective-version))
                  (scm (string-append out "/share/guile/site/" version))
                  (go (string-append out "/lib/guile/" version "/site-ccache"))
                  (guile (which "guile")))
             (substitute* (string-append out "/bin/bonnie-bee")
               (("exec guile" cmd)
                (string-append "export GUILE_LOAD_PATH=\""
                               scm ":"
                               (getenv "GUILE_LOAD_PATH")
                               "\"\n"
                               "export GUILE_LOAD_COMPILED_PATH=\""
                               go ":"
                               (getenv "GUILE_LOAD_COMPILED_PATH")
                               "\"\n"
                               "exec " guile)))))))))
  (native-inputs (list autoconf automake pkg-config))
  (inputs (list target-guile))
  (propagated-inputs (list starling))
  (synopsis "Autum Lisp Game Jam 2021 entry")
  (description "Dave's Autumn Lisp Game 2021 entry.")
  (home-page "https://git.dthompson.us/autumn-lisp-game-jam-2021.git")
  (license license:gpl3+))
